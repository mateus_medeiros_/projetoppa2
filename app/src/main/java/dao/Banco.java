package dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Mateus on 08/06/2016.
 */
public class Banco extends SQLiteOpenHelper {

    //------------------------------Table atributos da base-----------------------------------
    public static final String TBL_USUARIO = "usuario";
    public static final String USUARIO_ID = "id";
    public static final String USUARIO_NOME = "nome";
    public static final String USUARIO_EMAILLOGIN = "email";
    public static final String USUARIO_SENHA = "senha";
    //-----------------------------------------------------------------------------
    public static final String TBL_CARRO = "carro";
    public static final String CARRO_ID = "id";
    public static final String CARRO_NOME = "nome";
    public static final String CARRO_KML = "kml";
    public static final String CARRO_POTENCIA = "potencia";
    //-----------------------------------------------------------------------------
    public static final String TBL_FABRICANTE = "fabricante";
    public static final String FABRICANTE_ID = "id";
    public static final String FABRICANTE_NOME = "nome";
    //-----------------------------------------------------------------------------
    public static final String TBL_FABRICANTECARRO = "fabricantecarro";
    public static final String FABRICACARRO_ID = "id";
    public static final String FABRICACARRO_ID_FABRICANTE = "idfabricante";
    public static final String FABRICACARRO_ID_CARRO = "idcarro";

    //------------------------------Tables-----------------------------------

    //Estrutura da tabela Agenda (sql statement)
    private static final String CREATE_USUARIO = "create table " +
            TBL_USUARIO + "( " + USUARIO_ID       + " integer primary key autoincrement, " +
                                USUARIO_NOME     + " text not null, " +
                                USUARIO_EMAILLOGIN + " text not null, " +
                                USUARIO_SENHA + " text not null);";


    private static final String CREATE_CARRO ="create table " +
                    TBL_CARRO + "( " + CARRO_ID       + " integer primary key autoincrement, " +
                                        CARRO_NOME     + " text not null, " +
                                        CARRO_KML     + " text not null, " +
                                        CARRO_POTENCIA + " text not null);";

    private static final String CREATE_FABRICANTE ="create table " +
            TBL_FABRICANTE + "( " + FABRICANTE_ID       + " integer primary key autoincrement, " +
                                    FABRICANTE_NOME + " text not null);";

    private static final String CREATE_FABRICANTECARRO ="create table " +
            TBL_FABRICANTECARRO + "( "  + FABRICACARRO_ID       + " integer primary key autoincrement, " +
                                         FABRICACARRO_ID_FABRICANTE       + " integer not null," +
                                         FABRICACARRO_ID_CARRO + " integer not null);";


    //-----------------------------------------------------------------------------

            private static final String DATABASE_NAME = "bancoteste.db";
            private static final int DATABASE_VERSION = 1;
    //-----------------------------------------------------------------------------


    public Banco(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase database) {
        //Criação da tabela
        database.execSQL(CREATE_USUARIO);
        database.execSQL(CREATE_CARRO);
        database.execSQL(CREATE_FABRICANTE);
        database.execSQL(CREATE_FABRICANTECARRO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Caso seja necessário mudar a estrutura da tabela
        //deverá primeiro excluir a tabela e depois recriá-la
        db.execSQL("DROP TABLE IF EXISTS " + TBL_USUARIO);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_CARRO);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_FABRICANTE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_FABRICANTECARRO);
        onCreate(db);
    }

}
