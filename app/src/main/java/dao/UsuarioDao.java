package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import entity.Carro;
import entity.Usuario;

/**
 * Created by Mateus on 24/05/2016.
 */
public class UsuarioDao {

    private SQLiteDatabase database;
    private Banco dbHelper;

    //Campos da tabela
    private String[] colunas = {Banco.USUARIO_ID,
            Banco.USUARIO_NOME,
            Banco.USUARIO_EMAILLOGIN,
            Banco.USUARIO_SENHA };

    public UsuarioDao(Context context) {
        dbHelper = new Banco(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void insert(Usuario usuario) {
        ContentValues values = new ContentValues();

        open();

        //Carregar os valores nos campos do Contato que será incluído
        values.put(Banco.USUARIO_NOME, usuario.getNome());
        values.put(Banco.USUARIO_EMAILLOGIN, usuario.getEmailLogin());
        values.put(Banco.USUARIO_SENHA, usuario.getSenha());

        database.insert(Banco.TBL_USUARIO, null, values);

        close();
    }


    public int update(Usuario usuario) {
        long id = usuario.getId();
        ContentValues values = new ContentValues();

        //Carregar os novos valores nos campos que serão alterados
        values.put(Banco.USUARIO_NOME, usuario.getNome());
        values.put(Banco.USUARIO_EMAILLOGIN, usuario.getEmailLogin());
        values.put(Banco.USUARIO_SENHA, usuario.getSenha());

        //Alterar o registro com base no ID
        //return database.update(Banco.TBL_USUARIO, values, Banco.USUARIO_ID + " = " + id, null);
        return 0;
    }

    public void delete(Usuario usuario) {
        long id = usuario.getId();

        //Exclui o registro com base no ID
        //database.delete(Banco.TBL_USUARIO, Banco.USUARIO_ID + " = " + id, null);
    }


    public ArrayList<Usuario> getAllUsu() {
        open();

        ArrayList<Usuario> listUsu = new ArrayList<Usuario>();

        Cursor cursor = database.query(Banco.TBL_USUARIO, null, null, null, null, null, Banco.USUARIO_ID);

        while (cursor.moveToNext()) {
            Usuario usu = getUsuBy(cursor);

            if(usu!=null) {
                listUsu.add(usu);
            }
        }
        //Tenha certeza que você fechou o cursor
        cursor.close();

        return listUsu;
    }


    public Usuario verifyLoginSenha(Usuario u) {
        open();

        Usuario usuario = new Usuario();
        usuario = null;

        Cursor cursor = database.rawQuery("select * from usuario where email=? and senha=?",
                new String[]{u.getEmailLogin(), u.getSenha()});

        if(cursor.moveToFirst()){

            usuario = getUsuBy(cursor);

        }
        cursor.close();
        return usuario;
    }

    public Usuario getUser(Usuario u) {
        open();

        Usuario usuario = new Usuario();
        usuario = null;

        Cursor cursor = database.rawQuery("select * from usuario where email=?",
                new String[]{u.getEmailLogin()});

        if(cursor.moveToFirst()){

            usuario = getUsuBy(cursor);

        }
        cursor.close();
        return usuario;
    }




    public List<Usuario> getAllUsers() {
        open();
        List<Usuario> listUsu = new ArrayList<Usuario>();

        //Consulta para trazer todos os dados da tabela Agenda ordenados pela coluna Nome
        Cursor cursor = database.query(Banco.TBL_USUARIO, null, null, null, null, null, Banco.USUARIO_NOME);

        // Cursor cursor2 = database.rawQuery("select * from usuario where id=? and email=?",new String[]{"123","mat@test"});//query("select * from usuario",null);
        // Cursor cursor3 = database.rawQuery("select * from usuario where id=? and email=?", null);

        cursor.moveToFirst();
        while (cursor.moveToNext()) {
            Usuario usuario = getUsuBy(cursor);
            listUsu.add(usuario);
            cursor.moveToNext();
        }
        //Tenha certeza que você fechou o cursor
        cursor.close();
        return listUsu;
    }

    //Converter o Cursor de dados no objeto POJO ContatoVO
    private Usuario getUsuBy(Cursor cursor) {
        Usuario usuario = new Usuario();
        usuario.setId((int) cursor.getLong(0));
        usuario.setNome(cursor.getString(1));
        usuario.setEmailLogin(cursor.getString(2));
        usuario.setSenha(cursor.getString(3));
        return usuario;
    }


}
