package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.glados61.mp.projetoppa2.ListFabric;

import java.util.ArrayList;
import java.util.List;

import entity.Carro;
import entity.Fabricante;
import entity.Usuario;

/**
 * Created by Mateus on 09/06/2016.
 */
public class CarroDao {

    private SQLiteDatabase database;
    private Banco dbHelper;

    //Campos da tabela Agenda
    private String[] colunas = {Banco.CARRO_ID,
                                Banco.CARRO_NOME,
                                Banco.CARRO_KML,
                                Banco.CARRO_POTENCIA };


    //public void upgradeDataBase(){
    //  SQLiteDatabase db = dbHelper.getWritableDatabase();
    //  dbHelper.onUpgrade(database, db.getVersion(), db.getVersion() + 1 );
    //}

    public CarroDao(Context context) {
        dbHelper = new Banco(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public long insert(Carro carro) {
        ContentValues values = new ContentValues();

        open();
        values.put(Banco.CARRO_NOME, carro.getNome());
        values.put(Banco.CARRO_KML, carro.getKml());
        values.put(Banco.CARRO_POTENCIA, carro.getPotencia());

        Long id = database.insert(Banco.TBL_CARRO, null, values);

        close();
        return id;
    }


    public ArrayList<Carro> getAllCarros() {
        open();

        ArrayList<Carro> listCar = new ArrayList<Carro>();

        Cursor cursor = database.query(Banco.TBL_CARRO, null, null, null, null, null, Banco.CARRO_ID);

        while (cursor.moveToNext()) {
            Carro carro = getCarBy(cursor);

            if(carro!=null) {
                listCar.add(carro);
            }
        }
        //Tenha certeza que você fechou o cursor
        cursor.close();

        return listCar;
    }


    public boolean update(Carro carro) {
        Long id = carro.getId().longValue();
        ContentValues values = new ContentValues();

        open();
        //Carregar os novos valores nos campos que serão alterados
        values.put(Banco.CARRO_NOME, carro.getNome());
        values.put(Banco.CARRO_KML, carro.getKml());
        values.put(Banco.CARRO_POTENCIA, carro.getPotencia());

        database.update(Banco.TBL_CARRO, values, Banco.CARRO_ID + "=" + id, null);


        close();
        return true;
    }


    public Carro getCarro(Integer id) {
        open();

        Carro carro = new Carro();
        carro = null;

        Cursor cursor = database.rawQuery("select * from carro where id=?",
                new String[]{id.toString()});

        if(cursor.moveToNext()){
            carro = getCarBy(cursor);
        }
        cursor.close();
        return carro;
    }


    //Converter o Cursor de dados no objeto POJO ContatoVO
    private Carro getCarBy(Cursor cursor) {
        Carro carro = new Carro();
        carro.setId((int) cursor.getLong(0));
        carro.setNome(cursor.getString(1));
        carro.setKml(cursor.getString(2));
        carro.setPotencia(cursor.getString(3));

        return carro;
    }


}

