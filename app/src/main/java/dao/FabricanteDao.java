package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import entity.Carro;
import entity.Fabricante;
import entity.Usuario;

/**
 * Created by Mateus on 09/06/2016.
 */
public class FabricanteDao {

    private SQLiteDatabase database;
    private Banco dbHelper;

    //Campos da tabela Agenda
    private String[] colunas = {//Banco.FABRICANTE_ID,
            Banco.FABRICANTE_NOME};


    public FabricanteDao(Context context) {
        dbHelper = new Banco(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Long insert(Fabricante fabricante) {
        ContentValues values = new ContentValues();

        open();
        values.put(Banco.FABRICANTE_NOME, fabricante.getNome());

        Long id = database.insert(Banco.TBL_FABRICANTE, null, values);

        close();
        return id;
    }


    public Fabricante getFabricante(Fabricante f) {
        open();

        Fabricante fabricante = new Fabricante();
        fabricante = null;

        Cursor cursor = database.rawQuery("select * from fabricante where nome=?",
                new String[]{f.getNome()});

        if(cursor.moveToFirst()){
            fabricante = getFabBy(cursor);
        }
        cursor.close();
        return fabricante;
    }

    public ArrayList<Fabricante> getAllFabricante() {
        open();

        ArrayList<Fabricante> listFab = new ArrayList<Fabricante>();

        Cursor cursor = database.query(Banco.TBL_FABRICANTE, null, null, null, null, null, Banco.FABRICANTE_ID);

        while (cursor.moveToNext()) {
            Fabricante fabricante = getFabBy(cursor);

            if(fabricante!=null) {
                listFab.add(fabricante);
            }
        }
        //Tenha certeza que você fechou o cursor
        cursor.close();

        return listFab;
    }


    //Converter o Cursor de dados no objeto POJO ContatoVO
    private Fabricante getFabBy(Cursor cursor) {
        Fabricante f = new Fabricante();
        f.setId((int) cursor.getLong(0));
        f.setNome(cursor.getString(1));

        return f;
    }



}
