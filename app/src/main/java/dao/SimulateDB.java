package dao;

import android.content.Context;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import entity.Usuario;

/**
 * Created by MateusMed on 03/05/2016.
 */
public class SimulateDB {

    public List<Usuario> usuarios;

    public SimulateDB() {

    }

    public void initList(){

//        Usuario u1 = new Usuario("mateus@teste", "123");
//        Usuario u2 = new Usuario("pedro@teste", "123");
//        Usuario u3 = new Usuario("vitor@teste", "123");
//
//        this.usuarios = new ArrayList<Usuario>();
//
//        this.usuarios.add(u1);
//        this.usuarios.add(u2);
//        this.usuarios.add(u3);
    }


    public Boolean addUsuario(Usuario usuario){

        if(this.usuarios.add(usuario)){
            return true;
        }else{
            return false;
        }
    }

    public Boolean logar(Usuario usuario){
        return usuarios.contains(usuario);
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
}
