package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entity.Carro;
import entity.Fabricante;
import entity.Usuario;

/**
 * Created by Mateus on 09/06/2016.
 */
public class FabricanteCarroDao {

    private SQLiteDatabase database;
    private Banco dbHelper;

    //Campos da tabela Agenda
    private String[] colunas = {Banco.FABRICACARRO_ID,
                                Banco.FABRICACARRO_ID_CARRO,
                                Banco.FABRICACARRO_ID_FABRICANTE};

    public FabricanteCarroDao(Context context) {
        dbHelper = new Banco(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void insert(Integer idFab, Integer idCar) {
        ContentValues values = new ContentValues();

        open();
        values.put(Banco.FABRICACARRO_ID_CARRO, idCar);
        values.put(Banco.FABRICACARRO_ID_FABRICANTE, idFab);

        database.insert(Banco.TBL_FABRICANTECARRO, null, values);
        close();
    }



    public Fabricante getAllCarsByFabricante(Fabricante f, Context context) {

        CarroDao carroDao = new CarroDao(context);
        open();

        List<Carro> listaCar = new ArrayList<Carro>();

        //erro aqui.
        Cursor cursor = database.rawQuery("select * from fabricantecarro where id=?",
                new String[]{String.valueOf(f.getId())});
            cursor.moveToFirst();

        close();
            while (cursor.moveToNext()) {

                //    Map<Integer, Integer> map = getFabBy(cursor);
                //    Integer value = map.get(f.getId());

                Carro c = new Carro();
                //   c.setId(value);

                //  c =  carroDao.getCarro(c);

                listaCar.add(c);

                cursor.moveToNext();
            }
        f.setCarros(listaCar);
        cursor.close();


        return f;
    }


    public List<Integer> getCarroListByFabricante(Fabricante f) {
        open();

        List<Integer> list = new ArrayList<Integer>();

        Cursor cursor = database.rawQuery("select idcarro from fabricantecarro where idfabricante=?",
                new String[]{f.getId().toString()});

        while(cursor.moveToNext()){

            Integer idCarro = new Integer(getCarId(cursor));

            if(idCarro!=null) {
                list.add(idCarro);
            }
        }
        cursor.close();
        return list;
    }

    public List<Map<Integer, Integer>> getAllFabricCar() {
        open();
        List<Map<Integer, Integer>> list = new ArrayList<Map<Integer, Integer>>();

        Cursor cursor = database.query(Banco.TBL_FABRICANTECARRO, null, null, null, null, null, Banco.FABRICACARRO_ID_FABRICANTE);

        cursor.moveToFirst();
        while (cursor.moveToNext()) {
            Map<Integer, Integer> map = getFabBy(cursor);
            list.add(map);
            cursor.moveToNext();
        }
        //Tenha certeza que você fechou o cursor
        cursor.close();
        close();
        return list;
    }

    private Map<Integer, Integer> getFabBy(Cursor cursor) {

        Map<Integer, Integer> map = new HashMap<Integer, Integer>();

        map.put( (int) cursor.getLong(0),(int) cursor.getLong(1));

        return map;
    }

    private Integer getCarId(Cursor cursor) {
        return (int) cursor.getLong(0);
    }



}
