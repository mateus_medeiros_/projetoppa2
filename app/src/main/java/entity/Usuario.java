package entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.EditText;

import java.io.Serializable;

/**
 * Created by MateusMed on 26/04/2016.
 */
public class Usuario implements Serializable {

    private Integer id;
    private String nome;
    private String emailLogin;
    private String senha;


    public Usuario() {
    }


    public Usuario(Integer id, String nome, String emailLogin, String senha) {
        this.id = id;
        this.nome = nome;
        emailLogin = emailLogin;
        this.senha = senha;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmailLogin() {
        return emailLogin;
    }

    public void setEmailLogin(String emailLogin) {

        this.emailLogin = emailLogin;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public boolean equals(Object o) {

        if(o instanceof Usuario){

            if(((Usuario) o).getEmailLogin().equals(this.getEmailLogin()) &&
                    ((Usuario) o).getSenha().equals(this.getSenha())){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return emailLogin;
    }

}
