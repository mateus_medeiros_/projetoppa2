package entity;

/**
 * Created by MateusMed on 19/04/2016.
 */
public class Carro {

    private Integer id;
    private String nome;
    private String kml;
    private String potencia;

    public Carro() {
    }

    public Carro(Carro carro) {

        id = carro.getId();
        nome = carro.getNome();
        kml = carro.getKml();
        potencia = carro.getPotencia();

    }


    @Override
    public boolean equals(Object o) {

        if(o instanceof Carro
                &&
          this.getNome().equals(((Carro) o).getNome())
                &&
          this.getKml().equals(((Carro) o).getKml())
                &&
          this.getPotencia().equals(((Carro) o).getPotencia())){
            return true;

        }
        return false;
    }

    public Carro(Integer id, String nome, String kml, String potencia) {
        this.id = id;
        this.nome = nome;
        this.kml = kml;
        this.potencia = potencia;
    }

    public void setKml(String kml) {
        this.kml = kml;
    }

    public String getKml() {
        return kml;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPotencia() {
        return potencia;
    }

    public void setPotencia(String potencia) {
        this.potencia = potencia;
    }

    @Override
    public String toString() {
        return nome ;
    }
}
