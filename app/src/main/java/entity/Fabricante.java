package entity;

import java.util.List;

/**
 * Created by MateusMed on 19/04/2016.
 */
public class Fabricante {

    private Integer id;
    private String nome;
    private List<Carro> carros;

    public Fabricante() {
    }

    public Fabricante(Object o) {

        if(o instanceof Fabricante){

            this.id = ((Fabricante) o).getId();
            this.nome = ((Fabricante) o).getNome();
            this.carros = ((Fabricante) o).getCarros();

        }
    }

    public Fabricante(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Fabricante(Integer id, String nome, List<Carro> carros) {
        this.id = id;
        this.nome = nome;
        this.carros = carros;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Carro> getCarros() {
        return carros;
    }

    public void setCarros(List<Carro> carros) {
        this.carros = carros;
    }

    @Override
    public String toString() {
        return  nome ;
    }
}
