package com.glados61.mp.projetoppa2;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dao.CarroDao;
import dao.FabricanteCarroDao;
import dao.FabricanteDao;
import entity.Carro;
import entity.Fabricante;

/**
 * Created by MateusMed on 01/05/2016.
 */
public class Consulta extends Activity implements AdapterView.OnItemSelectedListener {

    private ArrayList<Fabricante> fabricantes;
    private Spinner spinner1;
    private Spinner spinner2;
    private AlertDialog alerta;

    public void setFabricantes() {

        //vou ter q fazer zuado por causa de polls de conexões
        //get fabricantes
        //get listaCarros para cada fabricante

        FabricanteDao fd = new FabricanteDao(this);
        ArrayList<Fabricante> listFabricante = fd.getAllFabricante();

        FabricanteCarroDao fcd = new FabricanteCarroDao(this);

        CarroDao cd = new CarroDao(this);

        for(Fabricante f : listFabricante){

            List<Integer> listCar = fcd.getCarroListByFabricante(f);

            List<Carro> carrosList = new ArrayList<Carro>();

                for (Integer id : listCar){

                    Carro c = cd.getCarro(id);

                    carrosList.add(c);
                }

            f.setCarros(carrosList);
        }

        fabricantes = listFabricante;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.consulta);

        setFabricantes();

        spinner1 = (Spinner) findViewById(R.id.fabricante);
        spinner2 = (Spinner) findViewById(R.id.modelo);

        ArrayAdapter<Fabricante> adapter = new ArrayAdapter<Fabricante>(this,
                android.R.layout.simple_spinner_item, fabricantes);
        spinner1.setAdapter(adapter);
        spinner1.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    public void setSpinner2(List<Carro> listaCar){

        ArrayAdapter<Carro> adapter = new ArrayAdapter<Carro>(this,
                android.R.layout.simple_spinner_item, listaCar);
        spinner2.setAdapter(adapter);

    }

    void alert(Fabricante fab, Carro car){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(fab.getNome());
        builder.setMessage(car.getNome());
        Toast.makeText(Consulta.this, "tente novamente", Toast.LENGTH_SHORT).show();
        alerta = builder.create();
        alerta.show();
    }

    void alert(Integer result){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(result.toString() + "L de custo");
        alerta = builder.create();
        alerta.show();
    }

    public Double custoTotal(Integer litros, Double valorGasolina){

        return (litros * valorGasolina);
    }


    public void voltarParaIndex(View v){

        Intent mainActivity = new Intent(this, MainActivity.class);

        startActivity(mainActivity);

    }


    public void calc(View v){
        Fabricante fab = (Fabricante) spinner1.getSelectedItem();
        Carro car = (Carro) spinner2.getSelectedItem();
        TextView textView =  (EditText) findViewById(R.id.km);
        Integer km = Integer.parseInt(textView.getText().toString());

        Integer kml = Integer.parseInt(car.getKml().toString());

        Integer result = (km / kml);

        Double resposta = custoTotal(result, 3.80);

        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        TextView textViewresult = (TextView) findViewById(R.id.result);
        textViewresult.setText("R$: " + decimalFormat.format(resposta).toString());

        alert(result);
        //alert(fab, car);
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {

        Fabricante sp1= new Fabricante(spinner1.getSelectedItem());
//        Fabricante sp2= new Fabricante(spinner2.getSelectedItem());

        if(sp1 != null){

            setSpinner2(sp1.getCarros());
        }


    }

}
