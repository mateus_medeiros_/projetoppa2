package com.glados61.mp.projetoppa2;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import dao.CarroDao;
import dao.FabricanteCarroDao;
import dao.FabricanteDao;
import entity.Carro;
import entity.Fabricante;

public class Inside extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private AlertDialog alerta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inside);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.inside, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void mockDb(){

        CarroDao carroDao = new CarroDao(this);

        Carro c = carroDao.getCarro(1);

        if(c == null) {

            Long id1 = carroDao.insert(new Carro(null, "Gol", "10", "1.6"));
            carroDao.insert(new Carro(null, "Ônix", "12", "1.6"));
            carroDao.insert(new Carro(null, "Uno", "9", "1.6"));
            carroDao.insert(new Carro(null, "Corolla", "15", "2.0"));
            carroDao.insert(new Carro(null, "Jetta", "9", "2.0"));
            carroDao.insert(new Carro(null, "Golf", "15", "2.0"));

            FabricanteDao fabricanteDao = new FabricanteDao(this);
            fabricanteDao.insert(new Fabricante(null, "Volkswagen"));
            fabricanteDao.insert(new Fabricante(null, "Chevrolet"));
            fabricanteDao.insert(new Fabricante(null, "Fiat"));
            fabricanteDao.insert(new Fabricante(null, "Toyota"));

            FabricanteCarroDao fabricanteCarroDao = new FabricanteCarroDao(this);
            fabricanteCarroDao.insert(1, 1);
            fabricanteCarroDao.insert(2, 2);
            fabricanteCarroDao.insert(3, 3);
            fabricanteCarroDao.insert(4, 4);
            fabricanteCarroDao.insert(1, 5);
            fabricanteCarroDao.insert(1, 6);

            msgok();
        }else{
            msgok();

        }

    }


    private void aqui() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("aqui");

        //cria o AlertDialog
        alerta = builder.create();
        //Exibe
        alerta.show();
    }

    private void msgok() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("syncOk");
        Toast.makeText(Inside.this, "=/", Toast.LENGTH_SHORT).show();

        //cria o AlertDialog
        alerta = builder.create();
        //Exibe
        alerta.show();
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.fabricantes) {
            aqui();
            // pq nao funciona¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿ maldito manifest
            Intent main = new Intent(this, ListFabric.class);
            startActivity(main);

        } else if (id == R.id.carros) {

            Intent main = new Intent(this, ListCarros.class);
            startActivity(main);


        } else if (id == R.id.usuarios) {

            Intent main = new Intent(this, ListUsuarios.class);
            startActivity(main);

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.syncdb) {
            mockDb();
        } else if (id == R.id.sair) {

            Intent main = new Intent(this, MainActivity.class);
            startActivity(main);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
