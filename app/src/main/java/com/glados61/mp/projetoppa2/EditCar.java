package com.glados61.mp.projetoppa2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import dao.CarroDao;
import entity.Carro;

/**
 * Created by Mateus on 11/06/2016.
 */
public class EditCar extends Activity{

    private AlertDialog alerta;
    private Carro c;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editcar);

        Intent xpto = getIntent();
        Integer id = (Integer) xpto.getSerializableExtra("id");


        CarroDao carroDao = new CarroDao(this);

        c = new Carro(carroDao.getCarro(id));

        TextView textViewnome = (TextView) findViewById(R.id.editText5);
        textViewnome.setText(c.getNome().toString());

        TextView textViewkml = (TextView) findViewById(R.id.editText6);
        textViewkml.setText(c.getKml().toString());

        TextView textViewPotencia = (TextView) findViewById(R.id.editText7);
        textViewPotencia.setText(c.getPotencia().toString());



    }


    void alert(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("aqui");

        alerta = builder.create();
        alerta.show();
    }


    public void voltar(View v){

        Intent edit = new Intent(this, Inside.class);
        startActivity(edit);

    }

    public void salvar(View v){

        CarroDao carroDao = new CarroDao(this);

        Carro carro = new Carro();

        carro.setId(c.getId());

        TextView textViewnome = (TextView) findViewById(R.id.editText5);
        carro.setNome(textViewnome.getText().toString());

        TextView textViewkml = (TextView) findViewById(R.id.editText6);
        carro.setKml(textViewkml.getText().toString());

        TextView textViewPotencia = (TextView) findViewById(R.id.editText7);
        carro.setPotencia(textViewPotencia.getText().toString());


        if(!carro.equals(c)) {
            carroDao.update(carro);
            Intent edit = new Intent(this, Inside.class);
            startActivity(edit);

        }else{

            alert();

        }

        //

    }

}
