package com.glados61.mp.projetoppa2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import dao.CarroDao;
import dao.FabricanteDao;
import entity.Carro;
import entity.Fabricante;

/**
 * Created by Mateus on 11/06/2016.
 */
public class ListCarros extends Activity {

    private ArrayList<Carro> carros;

    private AlertDialog alerta;
    ListView listView;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listfabric);

        context = this;

        EditCar editCar = new EditCar();

        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.listfabric);

        CarroDao carDao = new CarroDao(this);
        carros = carDao.getAllCarros();


        final ArrayAdapter<Carro> adapter = new ArrayAdapter<Carro>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, carros);

        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition     = position;


                // ListView Clicked item value
                Carro  itemCar = (Carro) listView.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :"+itemPosition+"  ListItem : " +itemCar , Toast.LENGTH_LONG)
                        .show();

                Intent edit = new Intent(context, EditCar.class);
                edit.putExtra("id", itemCar.getId());

                startActivity(edit);
            }

        });


    }

    public void method(){
            alert();
    }

    void alert(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("aqui");
        Toast.makeText(ListCarros.this, "tente novamente", Toast.LENGTH_SHORT).show();
        alerta = builder.create();
        alerta.show();
    }

    void alert(Fabricante f){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(f.toString());
        alerta = builder.create();
        alerta.show();
    }

}
