package com.glados61.mp.projetoppa2;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import dao.FabricanteDao;
import dao.UsuarioDao;
import entity.Fabricante;
import entity.Usuario;

/**
 * Created by Mateus on 11/06/2016.
 */
public class ListUsuarios extends Activity {

    private ArrayList<Usuario> usuarios;

    private AlertDialog alerta;
    private TextView textView;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listfabric);

        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.listfabric);

        UsuarioDao usuarioDao = new UsuarioDao(this);
        usuarios = usuarioDao.getAllUsu();

        ArrayAdapter<Usuario> adapter = new ArrayAdapter<Usuario>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, usuarios);

        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition     = position;

                // ListView Clicked item value
                Usuario  itemFab = (Usuario) listView.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :"+itemPosition+"  ListItem : " +itemFab , Toast.LENGTH_LONG)
                        .show();

            }

        });


    }

    public void method(){
            alert();
    }

    void alert(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("aqui");
        Toast.makeText(ListUsuarios.this, "tente novamente", Toast.LENGTH_SHORT).show();
        alerta = builder.create();
        alerta.show();
    }


}
