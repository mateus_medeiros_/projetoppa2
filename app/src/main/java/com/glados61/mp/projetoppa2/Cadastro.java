package com.glados61.mp.projetoppa2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import dao.UsuarioDao;
import entity.Usuario;

/**
 * Created by MateusMed on 01/05/2016.
 */
public class Cadastro extends Activity implements View.OnClickListener{

    private Usuario usuario;
    private AlertDialog alerta;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        usuario = new Usuario();
        setContentView(R.layout.cadastro);
    }


    public Boolean compare(String senha1, String senha2){

        if(senha1.equals(senha2)){
            return true;
        }
        return false;
    }


    public Boolean verifyUser(Usuario usuario){

        if(usuario.getNome() != null && !usuario.getNome().equals("") &&
           usuario.getEmailLogin() != null && !usuario.getEmailLogin().equals("") &&
           usuario.getSenha() != null && !usuario.getSenha().equals("")){

            return true;
        }
        return false;
    }

    private void alertCampos() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ops");
        builder.setMessage("algum de seus campos está vazio");
        Toast.makeText(Cadastro.this, "tente novamente", Toast.LENGTH_SHORT).show();

        //cria o AlertDialog
        alerta = builder.create();
        //Exibe
        alerta.show();
    }

    private void sucess() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //builder.setTitle("Ops");
        builder.setMessage("Parabens, você foi cadastrado");
        Toast.makeText(Cadastro.this, "sucess", Toast.LENGTH_SHORT).show();

        //cria o AlertDialog
        alerta = builder.create();
        //Exibe
        alerta.show();
    }


    private void alertSenhas() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ops");
        builder.setMessage("as senhas não batem");
        Toast.makeText(Cadastro.this, "tente novamente", Toast.LENGTH_SHORT).show();

        //cria o AlertDialog
        alerta = builder.create();
        //Exibe
        alerta.show();
    }



    @Override
    public void onClick(View v) {

        EditText nome = (EditText) findViewById(R.id.editText3);
        EditText email = (EditText) findViewById(R.id.editText4);
        EditText senha1 = (EditText) findViewById(R.id.editText);
        EditText senha2 = (EditText) findViewById(R.id.editText2);

        usuario.setNome(nome.getText().toString());
        usuario.setEmailLogin(email.getText().toString());
        usuario.setSenha(senha1.getText().toString());


        Intent intent = new Intent(this, MainActivity.class);

        switch (v.getId()){

            case R.id.voltarIndexCadastro:
                System.out.println("case1");
                startActivity(intent);
                break;

            case R.id.buttonCadastrar:

                if(verifyUser(usuario)==false){
                    alertCampos();
                    break;
                }

                if(compare(senha1.getText().toString(), senha2.getText().toString())){
                    UsuarioDao usuarioDao = new UsuarioDao(this);

                    usuarioDao.insert(usuario);
                    sucess();
                    startActivity(intent);
                }else{
                    alertSenhas();
                }
                break;
        }



    }
}
