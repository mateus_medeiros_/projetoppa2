package com.glados61.mp.projetoppa2;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import dao.CarroDao;
import dao.FabricanteCarroDao;
import dao.FabricanteDao;
import entity.Carro;
import entity.Fabricante;

/**
 * Created by Mateus on 11/06/2016.
 */
public class ListFabric extends Activity {

    private ArrayList<Fabricante> fabricantes;

    private AlertDialog alerta;
    private TextView textView;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listfabric);

        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.listfabric);

        FabricanteDao fabricanteDao = new FabricanteDao(this);
        fabricantes = fabricanteDao.getAllFabricante();

        ArrayAdapter<Fabricante> adapter = new ArrayAdapter<Fabricante>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, fabricantes);

        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition     = position;

                // ListView Clicked item value
                Fabricante  itemFab = (Fabricante) listView.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :"+itemPosition+"  ListItem : " +itemFab , Toast.LENGTH_LONG)
                        .show();

            }

        });


    }

    public void method(){
            alert();
    }

    void alert(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("aqui");
        Toast.makeText(ListFabric.this, "tente novamente", Toast.LENGTH_SHORT).show();
        alerta = builder.create();
        alerta.show();
    }

    void alert(Fabricante f){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(f.toString());
        alerta = builder.create();
        alerta.show();
    }

}
