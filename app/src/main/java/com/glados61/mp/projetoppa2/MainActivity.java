package com.glados61.mp.projetoppa2;


import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import dao.SimulateDB;
import dao.UsuarioDao;
import entity.Usuario;
import testes.Adapter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    //private Button button;
    public Usuario usuario;
    private AlertDialog alerta;

    // private TextView tStatus;

    public MainActivity() {
        usuario = new Usuario();
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id){

        Toast.makeText(this, "Clique no item :" + position, Toast.LENGTH_SHORT);
    }

    public void exempleListView(Adapter adapter){

        ListView listView_teste = (ListView) findViewById(R.id.listView);

        List<String> list = new ArrayList<String>();
        list.add("Item 1");
        list.add("Item 2");
        list.add("Item 3");
        list.add("Item 4");


        ArrayAdapter<String> dataAdapter = new
                ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        listView_teste.setAdapter(dataAdapter);

        //activity pra onde irá, é isso?
        //listView_teste.setOnItemClickListener(this);

    }

    // On create deverá mapear todos os campos e botoes da tela
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.index);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void alertError() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Ops");

        builder.setMessage("login e senha invalidos");

        Toast.makeText(MainActivity.this, "tente novamente", Toast.LENGTH_SHORT).show();

        //cria o AlertDialog
        alerta = builder.create();
        //Exibe
        alerta.show();
    }

    private void procurandoUsu() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("procurando usuario...");
        Toast.makeText(MainActivity.this, "Sucesso!", Toast.LENGTH_SHORT).show();

        //cria o AlertDialog
        alerta = builder.create();
        //Exibe
        alerta.show();
    }

    private void tivemosProb() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("tivemos um prob ao encontrar o usuario");
        Toast.makeText(MainActivity.this, "verifique o login e a senha", Toast.LENGTH_SHORT).show();

        //cria o AlertDialog
        alerta = builder.create();
        //Exibe
        alerta.show();
    }



    public Boolean verifyUser(Usuario usuario){

        if(usuario.getEmailLogin() != null && !usuario.getEmailLogin().equals("") &&
                usuario.getSenha() != null && !usuario.getSenha().equals("")){

            return true;
        }
        return false;
    }




    @Override
    public void onClick(View v) {

        EditText login = (EditText) findViewById(R.id.inlog);
        EditText senha = (EditText) findViewById(R.id.inpassword);

        usuario.setEmailLogin(login.getText().toString());
        usuario.setSenha(senha.getText().toString());

        switch (v.getId()){
            case R.id.entrar:

                UsuarioDao usuarioDao = new UsuarioDao(this);

                List<Usuario> usuarioList = usuarioDao.getAllUsers();


                if(verifyUser(this.usuario)== true){

                    if(usuarioDao.verifyLoginSenha(usuario) != null){

                        Intent intentLogin = new Intent(this, Inside.class);


                        // Inside.start(this, usuario);

                        //intentLogin.putExtra("nome",  usuario.getNome());
                        //Intent intentLogin = new Intent(this, MapsActivity.class);
                        startActivity(intentLogin);
                    }else{
                        tivemosProb();
                    }

                }else{
                    alertError();
                }
                    break;

            case R.id.cadastrar:
                System.out.println("case2");
                Intent intentCadastro = new Intent(this, Cadastro.class);
                startActivity(intentCadastro);
                break;

            case R.id.consulta:
                System.out.println("case3");

                Intent intentConsulta = new Intent(this, Consulta.class);
                startActivity(intentConsulta);

                break;

        }



    }
}
