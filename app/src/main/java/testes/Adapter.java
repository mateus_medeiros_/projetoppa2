package testes;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

/**
 * Created by MateusMed e Pedro on 19/04/2016.
 */
public class Adapter extends BaseAdapter{

    private Context contex;

    public Adapter() {
    }

    public Adapter(Context contex) {
        super();
        this.contex = contex;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView t = new TextView(contex);
        return t;
    }
}
